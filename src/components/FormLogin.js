import React, { Component } from "react";

class FormLogin extends Component {
  state = {
    email: "",
    password: ""
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
      e.preventDefault();
      this.props.loginHandle(this.state.email, this.state.password);
  }

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <p>or use your email account</p>
        <input
          type="email"
          name="email"
          placeholder="ex. youremail@email.com"
          onChange={this.onChange}
          required
        ></input>
        <input
          type="password"
          name="password"
          placeholder="your password"
          onChange={this.onChange}
          required
        ></input>
        <span>
          forgot your password ? <a href="./login">click here</a>
        </span>
        <button type="submit">SIGN IN</button>
      </form>
    );
  }
}

export default FormLogin;
