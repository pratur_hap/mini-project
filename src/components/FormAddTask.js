import React, { Component } from "react";

class FormAddTask extends Component{
    
    state ={
        name:"",
        description:"",
        due_date:"2020-02-21"
    }


    handleTitleChange = e => {
        this.setState({[e.target.name]: e.target.value})
    }

    addNewTodo = async(e) => {
        e.preventDefault()
        try {
          let res = await fetch("https://sukatodo.herokuapp.com/api/v1/tasks/create", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Bearer " + JSON.parse(localStorage.getItem("userData")).token
            },
            body:JSON.stringify({  
              "name": this.state.name,
              "description": this.state.description,
              "due_date": this.state.due_date
            })
          })
          this.setState({name: "", description:""})
          await res.json()
          this.props.reloadTask()
        }catch(err) {
          console.log(err)
        }
      }


    render(){

        return(
            <form className="form-add" onSubmit={this.addNewTodo}>
                  <input type="text" name="name" placeholder="addtask..." onChange={this.handleTitleChange} value={this.state.name}required></input>
                  <input type="text" name="description" placeholder="description..." onChange={this.handleTitleChange} value={this.state.description} required></input>
            <button type="submit">+</button>
            </form>  
        )
    }
}

export default FormAddTask;