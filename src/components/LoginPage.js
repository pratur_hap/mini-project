import React, { Component } from "react";
import FormLogin from "./FormLogin"
import {Link, Redirect} from "react-router-dom";
import axios from "axios";  

class LoginPage extends Component {

  login = (email, password) =>{
    // ini adalah promise base ; promise return data berupa resolve(adalah then) atau reject(adalah catch)
    axios({
      method : "POST",
      url : "https://sukatodo.herokuapp.com/api/v1/users/login",
      // biasanya data : { email : email(dari parameter), password : password(dari parameter)}
      data : { email, password} 
    }).then(res  => {
      localStorage.setItem("userData",JSON.stringify(res.data.data));
      console.log(res.data.data)})
      return(
        <Redirect to="/" />
      )
    .catch(err => console.log(err))
  }

  render() {
    return (
      <div className="container">
        <div className="flex">
          <div className="sidebar">
            <div className="logo">LOGO</div>
            <h1 className="welcome">Hello, Friend</h1>
            <p className="side-para">
              Enter your personal details and start your journey with us
            </p>
            <Link to="/register">
            <button className="btn-primary">SIGN UP</button>
            </Link>
          </div>
          <div className="rightbar">
            <h1>Sign in to Task Dashboard</h1>
            <div className="social-media">
            <button><i className="fab fa-facebook-f"></i></button>
            <button><i className="fab fa-google-plus-g"></i></button>
            <button><i className="fab fa-linkedin-in"></i></button>
            </div>
            <FormLogin loginHandle={this.login}/>
          </div>
        </div>
      </div>
    );
  }
}
export default LoginPage;
