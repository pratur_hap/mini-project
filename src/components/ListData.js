import React, { Component } from "react";
import axios from "axios";

class ListData extends Component{
    render(){
        
        return(

            <li>
                <label>
                  <input type="checkbox" name="list-todo" value="" />
                    <span></span>
                </label>
                <div>
                <i className="fas fa-star" title="favorite"></i>
                <i className="fas fa-edit" title="edit"></i>
                <i className="fas fa-trash" title="delete"></i> 
                </div>
            </li>
        )
    }
}

export default ListData;