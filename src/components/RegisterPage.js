import React, { Component } from "react";
import {Link, Redirect} from 'react-router-dom';
import FormRegister from "./FormRegister";
import axios from "axios";

class RegisterPage extends Component {
    
  register = (name, email, password, password_confirmation) =>{
    // ini adalah promise base ; promise return data berupa resolve(adalah then) atau reject(adalah catch)
    axios({
      method : "POST",
      url : "https://sukatodo.herokuapp.com/api/v1/users",
      // biasanya data : { email : email(dari parameter), password : password(dari parameter)}
      data : { name, email, password, password_confirmation} 
    }).then(res  => {
      // localStorage.setItem("userData",JSON.stringify(res.data.data));
      console.log(res.data.data)})
      return(
        <Redirect to="/" />
      )
    .catch(err => console.log(err))
  }



  render() {

    return (
      <div className="container">
        <div className="flex">
          <div className="sidebar">
            <div className="logo">LOGO</div>
            <h1 className="welcome">Welcome Back!</h1>
            <p className="side-para">
              To keep connected with us please login with your personal info
            </p>
            <Link to="/login">
            <button className="btn-primary">SIGN IN</button>
            </Link>
            
          </div>
          <div className="rightbar">
            <h1>Create Account with</h1>
            <div className="social-media">
            <button><i className="fab fa-facebook-f"></i></button>
            <button><i className="fab fa-google-plus-g"></i></button>
            <button><i className="fab fa-linkedin-in"></i></button>
            </div>
            <FormRegister registerHandle={this.register}/>
            
          </div>
        </div>
      </div>
    );
  }
}
export default RegisterPage;
