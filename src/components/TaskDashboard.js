import React, { Component } from "react";
import FormAddTask from "./FormAddTask";
// import ListData from "./ListData";
import UserData from "./UserData";
import { Link } from "react-router-dom";
import axios from "axios";

class TaskDashboard extends Component {
  state = {
    todos: []
  };

  componentDidMount() {
    this.getDataTask();
  }

  getDataTask = async () => {
    await axios({
      method: "GET",
      url: "https://sukatodo.herokuapp.com/api/v1/tasks/show?page=1",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer " + JSON.parse(localStorage.getItem("userData")).token
      }
    })
      .then(res => {
        this.setState({
          todos: res.data.data.docs
        });
         console.log(res.data.data);
      })
      .catch(err => console.log(err));
  };

  deleteTask = async id => {
    try {
      await axios({
        method: "DELETE",
        url: "https://sukatodo.herokuapp.com/api/v1/tasks/delete/" + id,
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("userData")).token
        }
      });
      this.getDataTask();
    } catch (err) {
      console.log(err);
    }
  };

  importanceTask = async (id, importance) => {
    try {
      await axios({
        method: "PUT",
        url:
          "https://sukatodo.herokuapp.com/api/v1/tasks/update/" +
          id +
          "/importance",
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("userData")).token
        },
        
        data: {
          importance: !importance
        }
      });
      console.log("berhasil update")
      this.getDataTask();
    } catch (err) {
      console.log(err);
    }
  };

  completeTask = async (id, importance) => {
    try {
      await axios({
        method: "PUT",
        url:
          "https://sukatodo.herokuapp.com/api/v1/tasks/update/" +
          id +
          "/complete",
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + JSON.parse(localStorage.getItem("userData")).token
        },
        
        data: {
          completion: !importance
        }
      });
      console.log("berhasil update")
      this.getDataTask();
    } catch (err) {
      console.log(err);
    }
  };



  render() {
    if (this.state.todos.length <= 0) return <div className="loader"></div>;

    const yourname = JSON.parse(localStorage.getItem("userData"));
    return (
      <div className="container">
        {this.state.messege ? <div>this.state.message</div> : ""}
        <div className="navbar">
          <div className="logo">Logos</div>
          <Link to="/login">
            <div className="link">Sign Out</div>
          </Link>
        </div>

        <div className="flexdash">
          <div className="sidebar">
            <UserData handleUserData={yourname} />

            <div className="menu-left">
              <button>
                My Day <i className="fas fa-list"></i>
              </button>
              <button>
                Important <i className="fas fa-star"></i>
              </button>
              <button>
                Completed <i className="fas fa-check-square"></i>
              </button>
            </div>
          </div>

          <div className="rightbar">
            <div>
              <FormAddTask reloadTask={this.getDataTask} />
            </div>

            <ul>
              {this.state.todos.map(item => (
                <li key={item._id}>
                  <label 
                  style={
                    item.completion === true
                      ? { textDecoration: "line-through" }
                      : { textDecoration: "none" }
                  }
                  onClick={() => this.completeTask(item._id, item.completion)}>
                    <input type="checkbox" name="list-todo" value="" />
                    <span>{item.name}</span>
                  </label>
                  <p>{item.description}</p>
                  <div>
                    <i
                      className="fas fa-star"
                      style={
                        item.importance === true
                          ? { color: "#E17A47" }
                          : { bordercolor: "#6A63DD" }
                      }
                      onClick={() => this.importanceTask(item._id, item.importance)}
                      title="favorite"
                    ></i>
                    <i className="fas fa-edit" title="edit"></i>
                    <i
                      className="fas fa-trash"
                      title="delete"
                      onClick={() => this.deleteTask(item._id)}
                    ></i>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}
export default TaskDashboard;
