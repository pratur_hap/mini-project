import React, { Component } from "react";

class UserData extends Component{
    render(){
        return(
            <div className="userinfo">
              <img src="https://pngimage.net/wp-content/uploads/2018/05/admin-png-8.png" alt="adminpics"></img>
              <h4>{this.props.handleUserData.name}</h4>
        <h5>{this.props.handleUserData.email}</h5>
            </div>
        )
    }
}

export default UserData;