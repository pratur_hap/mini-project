import React, { Component } from 'react'
import {BrowserRouter as Router, Route} from "react-router-dom";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import TaskDashboard from "./TaskDashboard";


class MainPage extends Component {

    state={
        token:''
    }

    componentDidMount=()=>{
        this._cekToken();
    }

    _cekToken = async() => {
        let getToken = await localStorage.getItem("userData");
        // console.log(getToken);
        if(getToken !== null){
            this.setState({token: getToken})
        }
    }

    render() {
        // console.log(this.state.token)
        if(this.state.token!==''){
            return(
                <Router>
                    <Route path="/" exact={true}>
                        <TaskDashboard/>
                    </Route>
                </Router>
            )
        }
        return (
            <Router>
                <Route path="/login" exact={true}>
                    <LoginPage/>
                </Route>
                <Route path="/register" exact={true}>
                    <RegisterPage/>
                </Route>
            </Router>
            // <Router>
                // <Route path="/login" exact={true}>
                //     <LoginPage/>
                // </Route>
                // <Route path="/register" exact={true}>
                //     <RegisterPage/>
                // </Route>
                // <Route path="/" exact={true}>
                //     <TaskDashboard/>
                // </Route>
            // </Router>
        )
    }
}

export default MainPage;
