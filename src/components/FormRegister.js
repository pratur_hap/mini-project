import React, { Component } from "react";

class FormRegister extends Component{

  state = {
    name:"",
    email: "",
    password: "",
    password_confirmation:""
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
      e.preventDefault();
      this.props.registerHandle(this.state.name, this.state.email, this.state.password, this.state.password_confirmation);
  }

    render(){
        return(
            <form onSubmit={this.onSubmit}>
            <p>or use your email for registration</p>
            <input type="text" name="name" placeholder="Your Name" onChange={this.onChange} required></input>
            <input type="email" name="email" placeholder="ex. youremail@email.com" onChange={this.onChange} required></input>
            <input
              type="password"
              name="password"
              placeholder="your password"
              onChange={this.onChange}
              required
            ></input>
            <input
              type="password"
              name="password_confirmation"
              placeholder="confirm password"
              onChange={this.onChange}
              required
            ></input>      

            <button type="submit">SIGN UP</button>
            </form>
        )
    }
}

export default FormRegister;