import React, { Component } from 'react';
import MainPage from "./components/MainPage";
import "./assets/style/styles.scss";

class App extends Component {
  render() {
    return (
      <div className="bg-back">
        <MainPage/>
      </div>
    )
  }
}

export default App;
